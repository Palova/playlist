#ifndef SKUSKA_H
#define SKUSKA_H

#include <QtWidgets/QMainWindow>
#include "ui_skuska.h"
#include <QFile>
#include <QTextStream>
#include <QFileDialog>
#include <QListWidget>
#include <QStringList>
#include <string>
#include <qmessagebox.h>
#include <QListWidgetItem>

class Skuska : public QMainWindow
{
	Q_OBJECT

public:
	Skuska(QWidget *parent = 0);
	~Skuska();

	public slots:
	void click1();
	void click2();
	void click3();
	//void itemClicked(QListWidgetItem *item)
	

private:
	Ui::SkuskaClass ui;
};

#endif // SKUSKA_H
